<?php
require_once('animal.php');
require_once("frog.php");
require_once("ape.php");

$sheep = new Animal("shaun");
echo "RELEASE 0 <br>";
echo "Name = ".$sheep->name; // "shaun"
echo "<br>";
echo "Legs = ".$sheep->legs; // 4
echo "<br>";
echo "Cold blooded = ".$sheep->cold_blooded; // "no"*/
echo "<p></p>";


$kodok = new Frog("buduk");
echo "RELEASE 1 <br>";
echo "Name = ".$kodok->name; // "buduk"
echo "<br>";
echo "Legs = ".$kodok->legs; // 4
echo "<br>";
echo "Cold blooded = ".$kodok->cold_blooded; // "no"
echo "<br>";
echo $kodok->jump() ; // "hop hop"
echo "<p></p>";

$sungokong = new Ape("kera sakti");
echo "Name = ".$sungokong->name; // "kera sakti"
echo "<br>";
echo "Legs = ".$sungokong->kaki; // 2
echo "<br>";
echo "Cold blooded = ".$sungokong->cold_blooded; // "no"
echo "<br>";
echo $sungokong->yell();// "Auooo"
echo "<p></p>";
?>